// Based on the Paho C code example from www.eclipse.org/paho/
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include "/home/pi/paho.mqtt.c/src/MQTTClient.h"
#define  CPU_TEMP "/sys/class/thermal/thermal_zone0/temp"

#include "ADXL345.h"
#include <unistd.h>
#include <pthread.h>

#include <sys/time.h>



using namespace std;
using namespace exploringRPi;
using namespace std;

//Please replace the following address with the address of your server
#define ADDRESS    "tcp://192.168.43.239:1883"
#define CLIENTID   "rpi1"
#define AUTHMETHOD "sarvesh"
#define AUTHTOKEN  "thisWillWork"
#define TOPIC      "ee513/CPUTemp"
#define QOS        2
#define TIMEOUT    10000L

float getCPUTemperature() {        // get the CPU temperature
   int cpuTemp;                    // store as an int
   fstream fs;
   fs.open(CPU_TEMP, fstream::in); // read from the file
   fs >> cpuTemp;
   fs.close();
   return (((float)cpuTemp)/1000);
}

int main(int argc, char* argv[]) {
   char str_payload[100];          // Set your max message size here
   MQTTClient client;
   MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
   MQTTClient_message pubmsg = MQTTClient_message_initializer;
   MQTTClient_deliveryToken token;
   MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
   opts.keepAliveInterval = 20;
   opts.cleansession = 1;
   opts.username = AUTHMETHOD;
   opts.password = AUTHTOKEN;
  int rc;
   if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
      cout << "Failed to connect, return code " << rc << endl;
      return -1;
   }
   
   ADXL345 mysensor(1,0x53);
   mysensor.setResolution(ADXL345::NORMAL);
   mysensor.setRange(ADXL345::PLUSMINUS_4_G);
   //usleep(100000);
   //mysensor.displayPitchAndRoll(2);
   
   MQTTClient_willOptions wopts = MQTTClient_willOptions_initializer;

   sprintf(str_payload, "{\"d\":{\"CPUTemp\": %f, \"Roll\": %f, \"Pitch\": %f }}", getCPUTemperature(), mysensor.displayRoll(), mysensor.displayPitch());
   pubmsg.payload = str_payload;
   pubmsg.payloadlen = strlen(str_payload);
   pubmsg.qos = QOS;
   pubmsg.retained = 0;
   MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
   cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<
        " seconds for publication of " << str_payload <<
        " \non topic " << TOPIC << " for ClientID: " << CLIENTID << endl;
   rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
   cout << "Message with token " << (int)token << " delivered." << endl;
   MQTTClient_disconnect(client, 10000);
   MQTTClient_destroy(&client);
   return rc;

}

