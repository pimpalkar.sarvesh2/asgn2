

#include <iostream>
#include "ADXL345.h"
#include <unistd.h>
#include <pthread.h>

using namespace std;
using namespace exploringRPi;

int main() {
   ADXL345 mysensor(1,0x53);
   mysensor.setResolution(ADXL345::NORMAL);
   mysensor.setRange(ADXL345::PLUSMINUS_4_G);
   usleep(100000);
   mysensor.displayPitchAndRoll();
 
   return 0;
}
